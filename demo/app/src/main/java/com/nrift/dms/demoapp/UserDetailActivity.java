package com.nrift.dms.demoapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
/**
 * Created by rahulc on 20.8.15.
 */
public class UserDetailActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "user_name";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.nrift.dms.demoapp.R.layout.activity_detail);

        Intent intent = getIntent();
        final String userName = intent.getStringExtra(EXTRA_NAME);

        final Toolbar toolbar = (Toolbar) findViewById(com.nrift.dms.demoapp.R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(com.nrift.dms.demoapp.R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(userName);

        loadBackdrop();
    }

    private void loadBackdrop() {
        final ImageView imageView = (ImageView) findViewById(com.nrift.dms.demoapp.R.id.backdrop);
        Glide.with(this).load(Data.getRandomUserImg()).centerCrop().into(imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.nrift.dms.demoapp.R.menu.sample_actions, menu);
        return true;
    }
}
