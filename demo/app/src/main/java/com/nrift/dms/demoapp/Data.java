package com.nrift.dms.demoapp;

import java.util.Random;
/**
 * Created by rahulc on 20.8.15.
 */
public class Data {

    private static final Random RANDOM = new Random();

    public static int getRandomUserImg() {
        switch (RANDOM.nextInt(5)) {
            default:
            case 0:
                return R.drawable.img1;
            case 2:
                return R.drawable.img2;
        }
    }

    public static final String[] userList = {
            "User 1", "User 2", "User 3", "User 4", "User 5",
            "User 6", "User 7", "User 8", "User 9"
    };

}
